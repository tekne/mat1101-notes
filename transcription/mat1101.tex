\documentclass{article}
\usepackage[margin=1in]{geometry}
\usepackage{mat1101}

\usepackage[backend=biber]{biblatex}
\addbibresource{references.bib}

\usepackage{hyperref}
\hypersetup{
  colorlinks,
  linkcolor={red!50!black},
  citecolor={blue!50!black},
  urlcolor={blue!80!black}
}

\title{MAT1101 Notes}
\author{Jad Elkhaleq Ghalayini}

\begin{document}

\maketitle

These notes are based off handwritten notes posted on Quercus by Professor Jim Arthur for MAT1101HS, along with material from Dummit and Foote. While I made my best efforts to ensure their contents are correct, there may be a variety of errors and inconsistencies, all of which are my own.

\tableofcontents
\newpage

We have
\begin{equation}\ker(\phi) = \{\sigma \in \Gal(KF'/F') : \sigma|_K = 1\} = \{1\}\end{equation}
i.e. \(\phi\) is injective. Set
\begin{equation}H = \im(\phi) \subset \Gal(K/F)\end{equation}
and let \(K^H\) be the fixed field of \(H\).
\begin{claim}
\(K^H = K \cap F'\)
\end{claim}
\begin{proof}
Since \(H\) fixes \(F'\), we have \(K^H \supset K \cap F'\). On the other hand, \(K^HF'\) is fixed by \(\Gal(KF'/F')\). Any \(\sigma \in \Gal(KF'/F')\) fixes \(F'\) and acts on \(K\) by \(\sigma|_K\). But any \(\sigma|_K\) lies in \(H\) by definition, and hence fixes \(K^H \subset L\), again by definition. But \(K^HF'\) corresponds to the subspace \(\Gal(KF'/K^HF')\) of \(\Gal(KF'/F')\) under the Galois correspondence, so, \(K^HF'\) must equal \(F'\) by bijectivity of the Galois correspondence (the fundamental theorem of Galois theory).
Hence, we have \(K^H \subset F'\), which directly implies the desired result.
\end{proof}
Therefore, we have that \(H = \Gal(K/K \cap F')\), since the fundamental theorem asserts that \(H = \Gal(K/K^H)\), i.e. \(\phi\) is an isomorphism from \(\Gal(KF'/F')\) onto \(\Gal(K/K \cap F')\) as required, completing the proof.

\begin{corollary}
Suppose \(K/F\) is Galois and \(F'/F\) is any \textbf{finite} set. Then
\begin{equation}[KF':F] = [K:F][F':F]/[K\cap F':F]\end{equation}
\end{corollary}
\begin{proof}
By the proposition above, we have
\begin{equation}[KF' : F'] = [K : K \cap F']
  \implies [KF' : F]
  = [KF' : F'][F' : F]
  = [K : K \cap F'][F' : F]\end{equation}
\begin{equation}= [K : K \cap F'][K \cap F' : F][K \cap F' : F]^{-1}[F' : F]
  = [K : F][K \cap F' : F]^{-1}[F' : F]
\end{equation}
\end{proof}
\begin{proposition}
Let \(K_1, K_2\) be Galois extensions of \(F\). Then:
\begin{enumerate}[label=(\alph*)]

  \item \(K_1 \cap K_2 / F\) is Galois

  \item \(K_1K_2/F\) is Galois and \(\Gal(K_1K_2/F)\) is isomorphic to
  \begin{equation}H = \{(\sigma, \tau) \in \Gal(K_1/F) \times \Gal(K_2/F) : \sigma|_{K_1 \cap K_2} = \tau|_{K_1 \cap K_2}\}\end{equation}

\end{enumerate}
\end{proposition}
\begin{proof}
\begin{enumerate}[label=(\alph*)]

  \item Let \(p(x) \in F[x]\) be irreducible with root \(\alpha \in K_1 \cap K_2\). Then \(p(x)\) is separable and all its roots lie in \(K_1\) \underline{and} in \(K_2\) (by Theorem 29), i.e. they must all lie in \(K_1 \cap K_2\). It follows that, as in the proof of Theorem 29, \(K_1 \cap K_2\) must be Galois.

  \item Since each \(K_i/F\) is Galois, it is the splitting field of a separable polynomial \(f_i(x) \in F[x]\). Then \(K_1K_2\) is tyhe splitting field of \(f(x) \in F[x]\) where \(f(x)\) is the ``squarefree" part of \(f_1(x)f_2(x)\) (i.e. take out multiple parts of the irreducible factors), a separable polynomial, so \(K_1K_2/F\) is Galois.

  Consider now the map \(\phi: \Gal(K_1K_2/F) \to \Gal(K_1/F) \times \Gal(K_2/F)\) given by \(\sigma \mapsto (\sigma|_{K_1}, \sigma|_{K_2})\). This is a well-defined homomorphism, since as \(K_i/F\) is Galois each \(\sigma|_{K_i}\) is some \(F\)-embedding \(K_i \to K_i'\) and hence can be taken as mapping \(K_i\) to itself. The kernel of this homomorphism consists of elements which are trivial on both \(K_1\) and \(K_2\) and hence trivial on \(K_1K_2\), so \(\phi\) is injective and therefore an isomorphism onto its image. The image lies in \(H\) as described in the statement of the proposition, since
  \begin{equation}(\sigma|_{K_1})_{K_1 \cap K_2} = \sigma|_{K_1 \cap K_2} = (\sigma|_{K_2})|_{K_1 \cap K_2}\end{equation}
  It remains to show that \(H\) is equal to the image image: to do so, we calculate \(|H|\): for any \(\sigma \in \Gal(K_1/F)\), there are \(|\Gal(K_2/K_1 \cap K_2)|\) elements \(\tau \in \Gal(K_2/F)\) such that
  \begin{equation}\tau|_{K_1 \cap K_2}
  = \sigma|_{K_1 \cap K_2} \implies |H|
  = |\Gal(K_1/F)||\Gal(K_2/K_1 \cap K_2)|\end{equation}
  \begin{equation} = |\Gal(K_1/F)|\frac{||\Gal(K_2/F)|}{|\Gal(K_1 \cap K_2/F)|}
  = [K_1K_2 : F]\end{equation}
  Since \([K_1K_2 : F] = |\Gal(K_1K_2/F)|\), and \(\Gal(K_1K_2/F) \subset H\), we must have \(\Gal(K_1K_2/F) = H\) as desired.

\end{enumerate}
\end{proof}
\begin{corollary}
Let \(K_1, K_2\) be Galois extensions of a field \(F\) with \(K_1 \cap K_2 = F\). Then
\begin{equation}\Gal(K_1K_2/F) = \Gal(K_1/F) \times \Gal(K_2/F)\end{equation}
Conversely, if \(K/F\) is Galois and \(G = \Gal(K/F) = G_1 \times G_2\) then \(K\) is a composite \(K_1K_2\) of two Galois extensions \(K_1/F\) and \(K_2/F\) with \(K_1 \cap K_2 = F\) and Galois groups \(G_1, G_2\) respectively.
\end{corollary}
\begin{proof}
Check.
\end{proof}
\begin{corollary}
Let \(E/F\) be any finite, separable extension. Then \(E\) is contained in an extension \(K\) that is Galois over \(F\) and \textbf{minimal}, i.e. in the underlying final algebraic closure \(\ol{F}\) of \(K\), any other Galois extension of \(F\) containing \(E\) contains \(K\).
\label{cor:gcdef}
\end{corollary}
\begin{proof}
We can choose any Galois extension of \(F\) containing \(E\) (for example, the composite of the splitting fields of the minimal polynomials form a basis of \(E/F\), implying they are all separable since \(E\) is separable over \(F\)). Then take \(K\) to be the intersectuion of all such Galois extensions over \(F\) containing \(E\).
\end{proof}

\begin{definition}
The Galois extension \(K\) of \(F\) containing \(E\) defined in Corollary \ref{cor:gcdef} is called the \textbf{Galois closure} of \(E\) over \(F\).
\end{definition}

Recall that an extension \(K/F\) is simple if \(K = F(\theta)\) for some \(\theta \in K\), in which case \(\theta\) is called a primitive element for \(K\) over \(F\).

\begin{proposition}
Let \(K/F\) be finite. Then \(K = F(\theta)\) is simple if and only if there exist only finitely many subfields \(K \supset E \supset F\)
\label{pr:finsub}
\end{proposition}
\begin{proof}
Suppose \(K = F(\theta)\) and let \(K \supset E \supset F\). Let \(f(x) \in F[x]\) be the minimal polynomial for \(\theta\) over \(F\) and \(g(x) \in E[x]\) be the minimal polynomial for \(\theta\) over \(E\). Then \(g(x) | f(x)\) in \(E[x]\). Let \(E'\), \(F \subset E' \subset E\), be a field over \(F\) generated by the coefficients of \(g(x)\) (which lie in \(E\)).
Then \(g(x)\) is still the minimal polynomial for \(\theta\) over \(E'\). This implies
\begin{equation}[K : E] = \deg g(x) = [K : E']\end{equation}
from whic we get \(E' = E\). Thus, for all \(E\), there exists an irreducible monic factor \(g(x) \in E[x]\) whose coefficients generate \(E/F\). Since \(f(x)\) has only finitely many monic factors (over \(K[x]\)), there exist only finitely many \(E\).

Conversely, suppose there exist only finitely many \(E\) with \(K \supset E \supset F\). Assume first that \(F\) is finite. Then \(K = \field{p^m}\) for some \(p\) prime, \(m \in \nats\). Then, as \(\field{p^m}^* = \gen{\theta}\) is a cyclic group, it follows that \(\field{p^m} = \field{p}(\theta)\).

On the other hand, assume \(F\) is infinite. Since \(K/F\) is finitely generated, it is enough to show that \(F(\alpha, \beta)\) is simple for all \(\alpha, \beta \in K\). Consider subfields of the form
\begin{equation}F(\alpha + c\beta) \subset F(\alpha, \beta), \qquad c \in F\end{equation}
Since \(|F| = \infty\) and \(|\{F: F \subset E \subset F(\alpha, \beta)\}| < \infty\) (by assumption), it follows that there exist (infinitely many) \(c \neq c' \in F\) such that \(F(\alpha + c\beta) = F(\alpha + c'\beta)\). In particular, \(\alpha +  c\beta, \alpha + c'\beta\) both lie in \(F(\alpha + c\beta)\). Therefore,
\begin{equation}(c - c')\beta = (\alpha + c\beta) - (\alpha - c'\beta) \in F(\alpha, \beta)
\implies \beta \in F(\alpha + c\beta)
\implies \alpha = (\alpha + c\beta) - c\beta \in F(\alpha + c\beta)\end{equation}
\begin{equation}\implies F(\alpha, \beta) = F(\alpha + c\beta)\end{equation}
i.e. \(F(\alpha, \beta)\) is simple. We can repeat this result as needed with the generators of \(K\) to obtain that \(K\) is simple.
\end{proof}

\begin{theorem}[Primitive Element Theorem]
If \(K/F\) is finite and separable, \(K/F\) is simple. In particular, any finite extension of fields of characteristic zero is 0 (since it is separable).
\end{theorem}
\begin{proof}
Let \(L \supset K \supset F\) be the Galois closure of \(K/F\) (in some underlying algebraically closed field). Then any subfield \(K \supset E \supset F\) corresponds to a unique subgroup \(H \subset \Gal(K/F)\). That is, \(E = L^H\). Since there exist only finitely many \(H\), there exist only finitely many \(E\), and therefore \(K = F(\theta)\) by simple application of Proposition \ref{pr:finsub}.
\end{proof}

For example, set \(K = \ol{\field{p}}(x, y)\): the field of rational functions in \(x, y\) over \(\ol{\field{p}}\), the algebraic closure of \(\field{p}\), and let \(F = \ol{\field{p}}(x^p, y^p)\). One can show that \([K:F] = p^2\) (check by picking a basis) and also that the subfields
\begin{equation}F \subset E = F(x + cy) \subset K, \qquad c \in F\end{equation}
are all of degree \(p\) over \(F\) (using the fact that \((x + cy)^p = x^p + c^py^p \in F\)). If any of the fields \(E\) are equal, then as in the proof of Proposition \ref{pr:finsub}, we would get \(\ol{\field{p}}(x, y) = F(x + cy)\), contradicting the degree property above. Therefore, there exist infinitely many \(E\), so the finite extension \(E/F\) is not simple by Proposition \ref{pr:finsub}.

\newpage

\section{Cyclotomic Extensions (again)}

Recall that \(\rationals(\zeta_m) \supset \rationals\) is the cyclotomic field of \(n^{th}\) roots
\begin{equation}\mu_m = \{x \in \mbb{C} : x^m = 1\}\end{equation}
of units in \(\mbb{C}\) (a cyclic group of order \(m\)), and \(\zeta_m\) denotes any generator in \(\mu_m\) (i.e. a \textbf{primitive \(m^{th}\) root of unity}, i.e. an element in
\begin{equation}\muprim{m} = \{\zeta \in \mu_m : \ord(\zeta) = m\} = \{\zeta_m^\alpha = e^{2\pi i a}{m} : a \in \ints, 1 \leq a < m, (a, m) = 1\}\end{equation}
a set of order \(\phi(m)\). Recall also that \(\muprim{m}\) is the set of roots of \(\Phi_m\): the \textbf{\(m^{th}\) cyclotomic polynomial}, a monic irreducible polynomial in \(\ints[x]\) of degree \(\phi(m)\). Hence, \(\rationals(\zeta_m)\) is the splitting field of \(\Phi_m\).

Note that any automorphism \(\sigma\) of \(\rationals(\zeta_m)\) must map \(\muprim{m}\) to itself, so \(\sigma\) gives a permutation of \(\muprim{m}\). In particular, \(\sigma(\zeta_m) = \zeta_m^a\), for some integer \(1 \leq  a < m\) with \((a, m) = 1\). Conversely, we can define \(\sigma_a\) for any \(a\) with \((a, m) = 1\). \(\sigma_a\) extends to an automorphism of \(\rationals(\zeta_m)\) that depends only on the isomorphism class of \(a \mod m\).

\begin{theorem}
The Galois group of \(\rationals(\zeta_m)/\rationals\) is given explicitly by the isomorphism
\begin{equation}(\ints/m\ints)^* \to \Gal(\rationals(\zeta_m)/\rationals), \qquad a \mapsto \sigma_a\end{equation}
\end{theorem}
\begin{proof}
For any \(a\), \(\sigma_a\) extends to a unique automorphism of \(\rationals(\zeta_m)/\rationals\), so a map \(a \to \sigma_a\) is well-defined. The map is a homomorphism, since
\begin{equation}(\sigma_a\sigma_b)(\zeta_m) = \sigma_a(\zeta_m^b) = \zeta_m^{ab} = \sigma_{ab}(\zeta_m) \implies \sigma_a\sigma_b = \sigma_{ab}\end{equation}
The map is bijective, since every Galois automorphism of \(\rationals(\zeta_m)/\rationals\) equals \(\sigma_a\) to a unique \(a \mod m\). Therefore, the map \(a \to \sigma_a\) is an isomorphism by the discussion above.
\end{proof}
\begin{corollary}
Let \(m = p_1^{a_1}...p_k^{a_k}\) be the prime decomposition of \(m\). Then there is a ``natural" isomorphism
\begin{equation}\Gal(\rationals(\zeta_m)/\rationals) \to \prod_i\Gal(\rationals(\zeta_{p_i^{a_i}})/\rationals)\end{equation}
which under the isomorphism of the last theorem is the Chinese remainder theorem
\begin{equation}(\ints / m\ints)^* \simeq \prod_i(\ints / p^{a_i}\ints)^*\end{equation}
\label{cor:galchin}
\end{corollary}
\begin{proof}
\TODO{this}
\end{proof}
We remark that we have \(\rationals(\zeta_m) = \rationals(\zeta_{p_1^{a_1}})...\rationals(\zeta_{p_1^{a_k)}})\) from the proof of Corollary \ref{cor:galchin}, to go with the corresponding isomorphism in the assertion.

It is known that
\begin{equation}
\Gal(\rationals(\zeta_{p^a})/\rationals) \simeq (\ints/p\ints)^*
\end{equation}
is cyclic of order \(\phi(p^a) = p^a - p^{a - 1}\) if \(p\) is odd, with
\begin{equation}
  (\ints / 2^a\ints)^* \simeq \left\{\begin{array}{cl}
    (\ints / 2\ints) \times (\ints / 2^{a - 2}\ints) & \text{if} \ a \geq 3 \\
    (\ints / 2\ints) & \text{if} \ a = 2
  \end{array}\right.
\end{equation}
The case \(a = 1\) we already know, since \(\rationals(\zeta_p) = \field{p}\), we have \(\rationals(\zeta_p)^* = (\ints/p\ints)^*\), which is cyclic of order \(p - 1\).

Consider the simplest special case: \(m = p\) prime. Let \(H\) be a subgroup of \(\Gal(\rationals(\zeta_p)/\rationals)\) and set
\begin{equation}
  \alpha = \alpha_H = \sum_{\sigma \in H}\sigma\zeta_p
\end{equation}
If \(\tau \in H\), then we have
\begin{equation}
  \forall \sigma \in H, \tau^{-1}\sigma \in H \implies \tau\alpha = \sum_{\sigma \in H}\tau\sigma\zeta_p = \sum_{\sigma \in H}\sigma\zeta_p = \alpha
\end{equation}
Hence, \(\alpha\) lies in the fixed field \(K^H\) of \(H\) in \(K = \rationals(\zeta_p)\). In general, the mapping \(\tau \mapsto \tau\zeta_p\) is a bijection from \(G = \Gal(\rationals(\zeta_p)/\rationals)\) to the basis \(\{\zeta_p,...,\zeta_p^{p - 1}\}\) of \(\rationals(\zeta_p)/\rationals\). In particular, \(\alpha\) is a sum of a subset of this basis of order \(|H|\).

If \(\tau \in G \setminus H\), then \(\tau\alpha\) is also a sum of a subset of the basis of order \(|H|\). But this subset cannot include \(\zeta_p = 1\zeta_p\), since the element 1 does not lie in the subset \(\tau H\). Since \(1\) does lie in \(H\), we get \(\tau\alpha \neq \alpha\), i.e. \(\alpha\) is not fixed by \(\tau\). Therefore \(\rationals(\alpha) = K^H\), the fixed field of \(H\), giving
\begin{equation}
  \Gal(\rationals(\alpha)/\rationals) \simeq G/H
\end{equation}
The elements \(\{\sigma_H: H \subset G\}\) and their conjugates \(\{\tau\sigma_H\tau^{-1} \in G \setminus H\}\) (and their generalizations for \(\rationals(\zeta_m)\), for arbitrary \(m\)) are called the \textbf{periods} (\textbf{Galois periods}) of \(\zeta_m\) with respect to \(H\).

\TODO{example}

\begin{definition}[Abelian extension]
An extension \(K/F\) of fields is an \textbf{abelian extension} if \(K/F\) is Galois and \(\Gal(K/F)\) is abelian.
\end{definition}
In particular, we note that \(K/F = \rationals(\zeta_m)/\rationals\) is an Abelian extension. The last example seems very general. It gives a concrete way of constructing many abelian extensions of \(\rationals\) as subfields of fields \(\rationals(\zeta_m)\). How general is it? Well suppose \(A\) is an arbitrary finite abelian group. Then we can write
\begin{equation}
  A \simeq (\ints / m_1\ints) \times ... \times (\ints / m_k\ints), \qquad m_1,...,m_k \geq 2
\end{equation}
Is it the Galois group of an extension over \(\rationals\)? We will assume
\begin{theorem}[Dirichlet]
For any integer \(m\), there are infinitely many primes \(p\) with \(p \equiv 1 \mod m\)
\end{theorem}
With this, we can choose primes \(p_1,...,p_k\) such that \(p_i \equiv 1 \mod m_i\). Let \(m = p_1...p_k\), and let
\begin{equation}
K = \rationals(\zeta_m) \simeq \prod_i\rationals(\zeta_{p_i}) = \prod_iK_i, \implies G = \Gal(K/F) = \prod_i\Gal(\rationals(\zeta_{p_i})/\rationals) = \prod_iG_i =
\end{equation}
Writing \(G_i \simeq (\ints/(p_i - 1)\ints)\), by construction, we have \(m_i | (p_i - 1)\). Hence, \(G_i \simeq (\ints / (p_i - 1)\ints)\) has a cyclic subgroup \(H_i\) of order \((p_i - 1) / m_i\), with quotient
\[A_i = G_i / H_i \simeq (\ints / (p_i - 1)\ints_i) / (m_i\ints_i / (p_i - 1)\ints) \simeq (\ints / m_i\ints)\]
If \(E = K^H = K_1^{H_1}...K_k^{H^k} = E_1...E_k\), then
\begin{equation}
  \Gal(E/\rationals) \simeq G/H \simeq G_1/H_1 \times ... \times G_k/H_k = A_1 \times ... \times A_k = A
\end{equation}
Thus, the finite abelian group \(A\) is the Galois group of the abelian extension \(E/\rationals = \rationals(\zeta_m)^H/\rationals\) of \(\rationals\). This is a partial answer to the question ``is any finite group \(G\) equal to \(\Gal(E/\rationals)\) for \(E/\rationals\) Galois?" But we get much \underline{more}:
\begin{enumerate}

  \item By choosing cyclotomic fields
  \begin{equation}
    K = \rationals(\zeta_m) \simeq \prod_i\rationals(\zeta_{p_i^{a_i}}) \impliedby m = \prod_ip_i^{a_i}
  \end{equation}
  we get many Galois extensions \(E = K^H\) of \(\rationals\) with Abelian Galois groups \(G/H\).

  \item By considering Galois periods \(\sigma_H = \sum_{\sigma \in H}\sigma\zeta_m\), as in the last example, we might expect to get explicit information about the polynomials in \(\rationals[x]\) of which \(E\) is the splitting field and the roots of these polynomials. This is harder in general than in the example.

\end{enumerate}
Underlying all this is
\begin{theorem}[Kronecker-Weber]
  Let \(E/\rationals\) be \underline{any} finite abelian extension. Then \(E\) is contained in some cyclotomic extension \(\rationals(\zeta_m)/\rationals\).
\end{theorem}
We write
\begin{equation}
  \rationals^{ab} = \bigcup_m\rationals(\zeta_m)
\end{equation}
``the most abelian extension of \(\rationals\)." From our earlier discussion of \(\varprojlim\) and \(\varinjlim\), one gets
\begin{equation}
  \rationals^{ab} = \varinjlim_m\rationals(\zeta_m) \implies \Gal(\rationals^{ab}/\rationals) = \varprojlim_m(\ints/m\ints)^* = \prod_p\varprojlim_{p^a}(\ints/p^a\ints) = \prod_p\ints_p
\end{equation}
the product of the rings of \(p\)-adic integers \(\ints_p\). Thus the \textbf{abelian class field theory over \(\rationals\)} says \(\rationals^{ab}\) is the maximal abelian extension of \(\rationals\), and that \(\Gal(\rationals^{ab}/\rationals) = \prod_p\ints_p\).

We make a few quick remarks on the subject:
\begin{enumerate}

  \item \textbf{General abelian class field theory}: if \(K/\rationals\) is any number field, there is also an explicit construction of \(\Gal(K^{ab}/K)\): the Galois group of maximal abelian extensions \(K^{ab}/K\) - but \underline{not} an explicit construction of \(K^{ab}\) itself, because we don't have a substructure for the fields \(\rationals(\zeta_m)\) (except for Kronecker's Jugendtraum: Hilbert's Twelfth Problem)

  \item \textbf{Nonabelian class field theory} (for any \(K\)): this is a central part of the Langlands program, and still a conjecture. It is closely tied to representation theory and nonabelian harmonic analysis.

\end{enumerate}

\newpage

\section{Commutative Algebra and Algebraic Geometry}

Roughly speaking, we will replace fields \(F\) by \underline{commutative} rings \(R\) with a unit \(1_R\) (an assumption on \(R\) in force from now on). The analogue of an algbraic extension \(K/F\) of fields is then an integral extension \(S/R\) of rings. The most basic case is to replace \(\rationals\) with \(\ints\).
\begin{definition}
Suppose \(R\) is a subring of a ring \(S\) (both commutative unit rings). Then
\begin{enumerate}

  \item \(s \in S\) is \textbf{integral over \(R\)} if it is a root of a monic polynomial in \(R[x]\).

  \item \(S\) is an \textbf{integral extension} of \(R\) (or just \textbf{integral over \(R\)}) if every \(s \in S\) is integral over \(R\).

  \item The \textbf{integral closure} of \(R\) in \(S\) is the set of elements of \(S\) that are integral over \(R\).

  \item \(R\) is \textbf{integrally closed in \(S\)} if \(R\) equals its integral closure in \(S\). The integral closure of an integral domain in \(R\) in its field of fractions is called the \textbf{normalization of \(R\)} and \(R\) is called \textbf{integrally closed} or \textbf{normal} if it is integrally closed in its field of fractions.

\end{enumerate}
\end{definition}
Let's look at some basic properties:
\begin{proposition}
Let \(R\) be a subring of \(S\), and \(s \in S\) (where \(R, S\) are commutative unit rings). Then the following are equivalent:
\begin{enumerate}

  \item \(s\) is integral over \(R\).

  \item The ring \(R[s]\) is a finitely generated \(R\)-module.

  \item \(s \in T\), for a ring \(T\), with \(R \subset T \subset S\) where \(T\) is a finitely generated \(R\)-module.

\end{enumerate}
\end{proposition}
\begin{corollary}
Let \(R\) be a subring of \(S\) and \(s, t \in S\) (where \(R, S\) are commutative unit rings). Then:
\begin{enumerate}

  \item If \(s, t\) are integral over \(R\), so are \(s \pm t\) and \(st\).

  \item The integral closure of \(R\) in \(S\) is a subring of \(S\) containing \(R\).

  \item If \(R \subset S \subset T\) where \(T\) is integral over \(S\) and \(S\) is integral over \(R\), then \(T\) is integral over \(R\).

  \item The integral closure of \(R\) in \(S\) is integrally closed in \(S\).

\end{enumerate}
\end{corollary}
\begin{proof}
\TODO{this}
\end{proof}

\begin{lemma}
Let \(S\) be an integral extension of \(R\) which is an integral domain. Then \(R\) is a field if and only if \(F\) is a field.
\end{lemma}
\begin{proof}
\TODO{this}
\end{proof}

\begin{definition}[\(k\)-algebra]
Suppose \(k\) is a field and \(A\) is a ring. We say that \(A\) is a \textbf{\(k\)-algebra} if \(k\) is contained in the center of \(A\) and the identity of \(k\) is the identity of \(A\). If there exist \(r_1,...,r_n \in A\) such that \(A = k[r_1,...,r_n]\), we say that \(A\) is a \textbf{finitely generated \(k\)-algebra}.
\end{definition}

\begin{definition}[Algebraically independent]
Suppose \(k\) is a field and \(S = A\) is a \(k\)-algebra. Then elements \(y_1,...,y_q \in A\) are \textbf{algebraically independent} over \(k\) if there does not exist a nonzero polynomial \(p \in k[x_1,...,x_q]\) such that
\begin{equation}
  p(y_1,...,y_q) = 0
\end{equation}
i.e. if the mapping \(k[x_1,...,x_q] \to A\) given by \(f(x_1,...,x_q) = f(y_1,...,y_q)\) is an isomorphism. See Dummit and Foote, page 645.
\end{definition}

\begin{theorem}[Noether Normalization Lemma]
Suppose \(k\) is a field and \(A = k[r_1,...,r_m]\) (with \(r_1,...,r_m \in A\)) is a finitely generated \(k\)-algebra. Then there exist algebraically independent elements \(y_1,...,y_q\), \(y_i \in A\), \(0 \leq q \leq m\) in \(A\) such that \(A\) is integral over \(k[y_1,...,y_q]\).
\end{theorem}
\begin{proof}
\TODO{this}
\end{proof}

Fix a field \(k\), which we will assume to be algebraically closed unless otherwise specified.
\begin{definition}[Affine \(m\)-space]
We define \textbf{affine \(m\)-space} to be
\begin{equation}
  \mbb{A}^m = \{(a_1,...,a_m) \in k^m\}
\end{equation}
We write \(k[\mbb{A}^m]\) to denote the ring of \(k\)-valued polynomials in \(m\) variables.
\end{definition}
\begin{definition}
If \(S \subset k[\mbb{A}^m]\), we define the \textbf{affine algebraic set defined by \(S\)}
\begin{equation}\mc{Z}(S) = \{(a_1,...,a_m) \in \mbb{A}^m : \forall f \in S, f(a_1,...,a_m) = 0\}\end{equation}
\end{definition}
\begin{definition}
If \(A \subset \mbb{A}^m\), we define \textbf{the ideal in \(k[\mbb{A}^m]\) defined by \(A\)}
\begin{equation}
  J(A) = \{f \in k[\mbb{A}^m] : \forall f (a_1,...,a_m) \in A, f(a_1,...,a_m) = 0\}
\end{equation}
\end{definition}
The maps \(S \to \mc{Z}(S)\), \(A \to J(A)\) satisfy various obvious properties: see Dummit and Foote, pages 659 to 661.

\subsection{Noetherian Rings}

Rings \(R\) (namely, commutative unit rings) are largely about \(R\)-ideals (and more generally, \(R\)-modules). We begin with a few basic concepts:
\begin{definition}[Noetherian ring]
A ring \(R\) is \textbf{Noetherian} if every \(R\)-ideal \(I \subset R\) is finitely generated, i.e. \(I = \gen{a_1,...,a_m}\) for \(a_1,...,a_m \in I\).
\end{definition}
\begin{theorem}[Hilbert basis theorem]
\label{thm:hilbasis}
If \(R\) is Noetherian, so is the ring \(R[x]\)
\end{theorem}
More generally,
\begin{definition}[Noetherian module]
An \(R\)-module \(M\) is Noetherian if every submodule \(N \subset M\) is finitely generated over \(R\).
\end{definition}
\begin{definition}[Maximal ideal]
An \(R\)-ideal \(I = M\) is \textbf{maximal} if it is maximal among the proper ideals in \(R\)
\end{definition}
\begin{proposition}
\(I\) is maximal if and only if \(R/I\) is a field.
\end{proposition}
\begin{definition}[Prime ideal]
An \(R\)-ideal \(I = P\) is \textbf{prime} if \(P \neq R\), and if whenever \(a, b \in R\) with \(ab \in P\) and \(a \notin P\), then \(b \in P\).
\end{definition}
\begin{proposition}
\(P\) is prime if and only if \(R/P\) is an integral domain
\end{proposition}
\begin{definition}
An \(R\)-ideal is \(I = Q\) is \textbf{primary} if \(Q \neq R\) and if whenever \(a, b \in R\) with \(ab \in Q\) and \(a \neq Q\), then there exists some \(m\) such that \(b^m \in Q\).
\end{definition}
\begin{proposition}
\(Q\) is primary if and only if every zero divisor in \(R/Q\) is nilpotent.
\end{proposition}
\begin{definition}
Suppose that \(I \subset R\) is an \(R\)-ideal. The \textbf{radical} of \(I\) is the ideal
\begin{equation}
\sqrt{I} = \{a \in R : \exists m \in \nats, a^m \in I\}
\end{equation}
The radical \(\sqrt{0}\) of the zero ideal is called the \textbf{nilradical} of \(R\), namely
\begin{equation}
\{a \in R : \exists m \in \nats, a^m = 0\}
\end{equation}
\end{definition}
\begin{proposition}
\(I\) is a \textbf{radical ideal} if and only if \(R/I\) has no nilpotent elements.
\end{proposition}
For example, consider \(R = \ints\). Then any \(R\)-ideal \(I\) equals \((a)\) for some \(a \in R\), since \(R\) is a principal ideal domain.
\begin{itemize}

  \item \(I = (a)\) is maximal if and only if \(I\) is prime, in which case \(a = p\) is prime in \(\ints\).

  \item \(I = (a)\) is primary if and only if \(a = p^m\), for \(p\) prime and \(m \in \nats\).

  \item \(I = (a)\) is a radical ideal if and only if \(a = p_1...p_k\) for distinct primes \(p_1,...,p_k\).

  \item If \(I = (p_1^{n_1}...p_k^{n_k})\) then \(\sqrt{I} = (p_1...p_k)\).

\end{itemize}
\begin{theorem}[Hilbert Nullstellensatz]
\label{thm:nullstell}
For any ideal (with \(k\) algebraically closed)
\begin{equation}
I \subset k[x_1,...,x_m]
\end{equation}
we have \(J(\mc{Z}(I)) = \sqrt{I}\). Moreover, the maps \(\mc{Z}\) and \(J\) give a bijective correspondence
\begin{equation}
\begin{tikzcd}
\{\text{affine algebraic sets}\} \arrow[r, "J"] & \{\text{radical ideals}\} \arrow[l, shift left, "\mc{Z}"]
\end{tikzcd}
\end{equation}
\end{theorem}
\begin{proof}
\TODO{this}
\end{proof}

\subsection{Operations on \(R\)-ideals}

We assume for this subsection that \(R\) is a Noetherian ring. We define the following operations on \(R\)-ideals:
\begin{definition}
For \(I, J \subset R\) ideals, we define their \textbf{sum} to be the ideal
\begin{equation}
  I + J = \{a + b: a \in I, b \in J\}
\end{equation}
\end{definition}
\begin{definition}
For \(I, J \subset R\) ideals, we define their \textbf{product} to be the ideal
\begin{equation}
  IJ = \left\{\sum_ia_ib_i: a_i \in I, b_i \in J\right\}
\end{equation}
Note this is the set of \text{sums of products}, \underline{not} necessarily individual products.
\end{definition}
\begin{proposition}
If \(I, J \subset R\) are ideals, then their set theoretic intersection \(I \cap J \subset R\) is also an ideal.
\end{proposition}
If \(R = \ints\), we have
\begin{equation}
  \forall I = (x), J = (y), IJ = I \cap J = (xy)
\end{equation}
In general, though, the product and intersection ideals are different. Products are easier to compute with, whereas intersections give a decomposition of ideals closer to the prime factorization of integers. For more, see \underline{Primary decomposition of ideals in Noetherian rings} on pages 681 to 685 of Dummit and Foote.

From now on \(k\) denotes an algebraically closed field. Recall that a subset \(V \subset \mbb{A}^m\) is an affine algebraic set if it is of the form \(\mc{Z}(S)\) for some \(S \subset k[\mbb{A}^m]\) (``the zero set of \(S\)"). We will describe some properties of such sets:
\begin{proposition}
  If \(S \subset T\), then \(\mc{Z}(T) \subset \mc{Z}(S)\)
\end{proposition}
\begin{proposition}
  If \(I = (S)\) is an ideal in \(k[\mbb{A}^m]\) generated by \(S\), then
  \(\mc{Z}(S) = \mc{Z}(I)\)
  \label{prop:idgenzero}
\end{proposition}
\begin{proposition}
  \(\mc{Z}(S) \cap \mc{Z}(T) = \mc{Z}(S \cup T)\). More generally, \textbf{any} intersection of algebraic sets is an algebraic set
  \begin{equation}
    \bigcap_i\mc{Z}(S_i) = \mc{Z}\left(\bigcup_iS_i\right)
  \end{equation}
  \label{prop:zerocaps}
\end{proposition}
\begin{proposition}
  For ideals \(I, J \in k[\mbb{A}^m]\),
  \(\mc{Z}(I) \cup \mc{Z}(J) = \mc{Z}(IJ)\)
  \label{prop:zerocups}
\end{proposition}
\begin{proposition}
  \(\mc{Z}(0) = \mbb{A}^m\), \(\mc{Z}(1) = \varnothing\).
  \label{prop:zeroids}
\end{proposition}

\begin{definition}[Zariski topology]
The \textbf{Zariski topology} on \(\mbb{A}^m\) is the topology where closed sets are affine algebraic subspaces \(V \subset \mbb{A}^m\). By Propositions \ref{prop:zerocaps}, \ref{prop:zerocups}, \ref{prop:zeroids}, it is actually a topology, which is \(T_0\) but \underline{not} \(T_1\).
For any affine algebraic set \(V \subset \mbb{A}^n\), the \textbf{Zariski topology on \(V\)} is the subspace topology induced by the Zariski topology on \(\mbb{A}^m\).
\end{definition}

Suppose \(V \subset \mbb{A}^m\) is any affine algebraic set. Then \(J(V)\) is the ideal of functions \(f \in k[\mbb{A}^m]\) that vanishes on \(A\). Since we can represent \(V = \mc{Z}(I)\) for an ideal \(I \subset k[\mbb{A}^n]\) by \ref{prop:idgenzero}, we get
\begin{equation}
  J(V) = J(\mc{Z}(I)) = \sqrt{I}
\end{equation}
by the Nullstellensatz (Theorem \ref{thm:nullstell}). In particular, \(J(V)\) is a radical ideal. Now since \(k[\mbb{A}^m] = k[x_1,...,x_m]\) is Noetherian by the Hilbert basis theorem (Theorem \ref{thm:hilbasis}), \(J(V)\) is finitely generated, i.e. \(J(V) = (f_1,...,f_m)\) for \(f_i \in k[x_1,...,x_m]\). Since \(V = \mc{Z}(J(V))\), we get
\begin{equation}
V = \{(x_1,...,x_m) \in \mbb{A}^m : f_1(x_1,...,x_m) = ... = f_m(x_1,...,x_m) = 0\}
\end{equation}
- the basic object in algebraic geometry.
\begin{definition}[Coordinate ring]
Set \(k[V] = k[\mbb{A}^m]/J(V)\). Since \(J(V)\) is the ideal of functions in \(k[\mbb{A}^m]\) that vanish on \(V\), any element in \(k[V]\) gives a well-defined function on \(V\), i.e. \(k[V]\) is a \(k\)-algebra of functions on \(V\), called the \textbf{coordinate ring of \(V\)}.
\end{definition}
\begin{definition}[Morphism]
Suppose now \(V, W \subset \mbb{A}^m\) are two affine algebraic sets. A map \(\phi: V \to W\) is called a \textbf{morphism} if
\begin{equation}
  \exists \phi_1,...,\phi_m \in k[x_1,...,x_m], \forall (a_1,...,a_m) \in V,
  \phi((a_1,...,a_m)) = (\phi_1(a_1,...,a_m),...,\phi_m(a_1,...,a_m))
\end{equation}
We call \(\phi\) an \textbf{isomorphism} if there exists a morphism \(\psi: W \to V\) such that \(\phi \circ \psi = \Id\), \(\psi \circ \phi = \Id\).
\end{definition}

\begin{theorem}
Given affine algebraic sets \(V, W \subset \mbb{A}^m\), there is a bijective correspondence between morphisms from \(V\) to \(W\) as algebraic affine sets and \(k\)-algebra homomorphisms from \(k[W]\) to \(k[V]\). More precisely,
\begin{enumerate}

  \item Every morphism \(\phi: V \to W\) induces an associated \(k\)-algebra homomorphism \(\phi^*: k[W] \to k[V]\) defined by \(\phi^*(f) = f \circ \phi\).

  \item Every \(k\)-algebra homomorphism \(\Phi: k[W] \to k[V]\) is induced by a unique morphism \(\phi: V \to W\), i.e. \(\Phi = \phi^*\).

  \item If \(\phi: V \to W\) and \(\psi: W \to U\) are morphisms, then
  \[(\psi \circ \phi)^* = \phi^* \circ \psi^*: k[U] \to k[V]\]

  \item The morphism \(\phi: V \to W\) is an isomorphism if and only if \(\phi^*: K[W] \to K[V]\) is a \(k\)-algebra isomorphism.

\end{enumerate}
\label{thm:morphmorph}
\end{theorem}
See Dummit and Foote, page 663.

\begin{definition}
A nonempty affine algebraic set \(V\) is \textbf{irreducible} if it cannot be written \(V = V_1 \cup V_2\) where \(V_1, V_2\) are proper algebraic subsets of \(V\). An irreducible affine algebraic set is called an \textbf{affine variety}.
\end{definition}

\begin{proposition}
\(V\) is irreducible if and only if \(J(V)\) is a prime ideal.
\end{proposition}
\begin{proposition}
Any \(V\) has a unique irreducible decomposition \(V = V_1 \cup ... \cup V_q\) for irreducible \(V_i\).
\end{proposition}
\begin{proposition}
Any radical ideal \(I\) has a unique, independent decomposition \(I = P_1 \cap ... \cap P_q\) for \(P_i\) prime. See Dummit and Foote, Proposition 12 on page 674 and Proposition 16 on page 680.
\end{proposition}
Theorem \ref{thm:morphmorph} specializes to:
\begin{corollary}
The category of affine algebraic varieties is equivalent to the category of finitely generated integral domains over \(k\).
\end{corollary}
See David Mumford, \underline{Introduction to Algebraic Geometry} (``the red book"). We remark that this is where Grothendieck kicks in: see Dummit and Foot, sections 15.4 and sections 15.5, and/or Mumford.

\end{document}
