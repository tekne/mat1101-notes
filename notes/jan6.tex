\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT1101 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{January 6 2020}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{mathabx}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}

\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\prt}[2]{{\frac{\partial {#1}}{\partial {#2}}}}
\def\ries{{\hat{\mbb{C}}}}
\newcommand{\reals}{\mbb{R}}
\newcommand{\nats}{\mbb{N}}
\newcommand{\ints}{\mbb{Z}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\DeclareMathOperator{\Res}{Res}

\begin{document}

\maketitle

Recall Algebra I focused on groups, rings and modules, covering chapters 1 through 12 of Dummit and Foote. In Algebra II, we'll continue to follow Dummit and Foote for chapters 13 to 17, and, as time permits, chapters 18 and 19, or possibly something out of Alperin and Bell. I did this last year and I'm going to do it again, as a little joke. So groups and rings are the primary topic of Algebra I. And I think most people, when they learn these topics, have a favorite: either they prefer groups, or they prefer rings. I work in groups, so I like groups better. So this is a question that may seem out of nowher: how many people here read Tolkien's \textit{The Lord of the Rings}? Rings play a big role in this book, in particular the one ring. What I'm going to say is a joke, but it's true.

There's a little rhyme in there: ``one ring to rule them all, one ring to find them, one ring to bring them all and in the darkness bind them." So I've just admitted to you that I prefer groups to rings. There's actually, believe it or not, an object that we don't understand, but we know it exists (or rather, everyone expects it exists), and its a group, and as encompassing, or overarching, as this ring is. So I'm going to take the liberty of modifying the above by writing group to get ``one group to rule them all, one group to find them, one group to bring them al and in the darkness bing them." Now in LotR darkness means evil, but here it just means that it brings things that we don't understand together and binds them.

The main topic of this course is Galois theory. If the idea of this group, which has been around for fifty years, pans out, then this one group exists, called the ``Motivic Galois Group," which is a part of algebraic geometry. Actually there's two other closely related groups, but I'm going to call this the one group. One is the ``Automoprhic Galois Group", proposed by Langlands, coming from analysis, believe it or not, specifically the group representations of automorphic forms. Now, the Motivic Galois Group, if its bringing things together in the darkness, they're motives.

It was thought of by the most abstract reasoning in mathematics, using category theory, by Alexander Grothendieck. He devised a way of breaking up algebraic varieties (manifolds which are kind of geometric objects), stating they have internal components we cannot see, very much like fundamental particles, and invented this motivic group to classify them. There's a third avatar of this group, which is more recent, with the language I'm using potentially undermining my credibility, known as the ``Cosmic Galois Group". If the first classifies automorphic representations, and the second classifies motives, then the third classifies the constants that physicists measure when they measure Feynman amplitudes, which is the heart of fundamental particle physics. This was more recently proposed by Pierre Cartier.

So, there really is a group that plays the role of the One Ring, though it's not evil (as far as we know), and not only is it very large and very abstract, but it has a great deal of very elaborate structure (and isn't just some floppy thing). It'll probably be a long road for us to get there, but it's fundamental.

In some sense, this Motivic Galois Group may be the final chapter in group theory, kind of the group that binds all groups. It's called a Galois group because it behaves like a Galois group, and in fact it has a piece of it that behaves like the largest Galois group, that is, the universal Galois group. If this might be the final chapter in group theory, then the first chapter, what would that be? Well, there's no question about that: it was invented by Galois, who invented the notion of a group, even though he didn't call it one. As you probably know, the reason that he did it was the preoccupation with much of the mathematical world, or at least the algebra or number theory side of it, with solving polynomial equations in terms of roots.

Galois was very ambitious: he wasn't just thinking about cubics or quartics or quintics, he was thinking about general equations. He posed himself this question: given
\[f(x) = \sum_{i = 0}^ma_ix^i, \qquad a_i \in \nats\]
If we count multiplicities, we know that there are \(n\) complex roots, counting multiplicities. But can we \textit{calculate them}. More precisely, can you express its roots \(\alpha_1,...,\alpha_n \in \mbb{C}\), which you know exist as complex numbers by successive extraction of square roots, or cube roots, or fourth roots, or \(k\)th roots.

About the time of Galois, it was suspected that this was impossible for the general quintic. In fact, I think it was proved by Legendre. But that didn't deter Galois, and he answered the question by inventing group theory. Of course, he didn't call it a group, but what he did was introduce a group, a finite group, in fact, what we could call a ``symmetry group" of \(f(x)\) as a subgroup of the symmetric group \(S_m\). Now what do I mean by a ``symmetry group"? You know that for a geometric figure like a square, the symmetry group for the shape would be the set of continuous bijections from the shape that preserve it, i.e. the set of linear transformations of \(\reals^3\) (for 3D shapes) which preserve the geometric figure. The icosahedron, for example, which I cannot draw but a model of which can be found in the cabinet outside, has the alternating group \(A_5\) as a symmetry group, whereas the tetrahedron has \(A_4\), and so on.

That notion of symmetry is relatively easy to imagine, if some of these symmetries may be hard to calculate. On the other hand, in the case of an equation, Galois defined the group \(\Gamma_f\), the Galois group of \(f\), to be the subgroup of the symmetric group to be the set of permutations \(\sigma \in S_n\) such that, having fixed an ordering of the roots \(\alpha_1,...,\alpha_n\),
\[\forall P \in \ints[x_1,...,x_m], P(\alpha_1,...,\alpha_m) \in \ints \implies P(\alpha_1,...,\alpha_m) = P(\alpha_{\sigma(1)},...,\alpha_{\sigma(m)})\]
This is not the way we do Galois theory now, we do it more systematically, but this is the concrete thing Galois had in his mind when he was defining the Galois group. If you think about this long enough, especially when you take the quadratic polynomials which you are very familiar with, it really is a sort of symmetry group.

All this new stuff, the Motivic Galois group, the Cosmic Galois group, are all based off symmetry, and when you go way way back, are hence based off Galois theory. The goal of the previous section was to get a feel for the original computational background of Galois theory, but now, we do it with fields. It turns out that it is much easier to describe if we set things up in a way dependent on what is happening with underlying fields.
The first part, and really the main part of the course, is going to be Galois theory. Specifically, the first part of this is field theory, based off Chapter 14 in Dummit and Foote.

I'm sorry for those of you that prefer rings. I like rings too, and they are the foundation of algebraic geometry, as we'll see at the end of the course, and they are very beautiful.

\end{document}
