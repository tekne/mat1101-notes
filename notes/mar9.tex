\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT1101 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{March 9 2020}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{mathabx}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}

\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\prt}[2]{{\frac{\partial {#1}}{\partial {#2}}}}
\def\ries{{\hat{\mbb{C}}}}
\newcommand{\reals}{\mbb{R}}
\newcommand{\nats}{\mbb{N}}
\newcommand{\ints}{\mbb{Z}}
\newcommand{\field}[1]{\mbb{F}_{#1}}
\newcommand{\incl}{\xhookrightarrow}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}
\newtheorem{corollary}{Corollary}

\DeclareMathOperator{\Res}{Res}
\DeclareMathOperator{\Gal}{Gal}

\begin{document}

\maketitle

We're going to do a little bit on the Galois groups of infinite field extensions, though I don't know if this is in the book. We will be working with the algebraic closure of a finite field, though in principle what we are doing could work with the algebraic closure \(\overline{\mbb{Q}}/\mbb{Q}\). Especially in things that come from arithmetic geometry, it is often convenient and even important to work with an infinite field extension. The bottom line is that the Galois group of the algebraic closure of a field (for us a finite field) is infinite, but its a topological group, i.e. it is not a discrete group, and is just a device, really, to be able to single out from that topological group finite quotients of it.

So, recall that we have algebraic closure
\[\overline{\field{p}} = \bigcup_{m \geq 1}\field{p^m}\]
So, the question is, what is \(\Gal(\overline{\field{p}}/\field{p})\)?

\begin{definition}[Direct limit]
Suppose \(\{A_i: i \in I\}\) is a family of algebraic objects (groups, rings, fields, etc.) parametrized by a \textbf{directed set} \(I\) (i.e. a partially ordered set such that any two elements have an upper bound). together with homomorphisms \(\phi_{ij}: A_i \to A_j\) for \(i \leq j\) such that
\begin{itemize}

  \item \(\varphi_{ik} = \varphi_{jk} \circ \varphi_{ij}\)

  \item \(\varphi_{ii} = \mb{1}\)

\end{itemize}
The direct limit is the algebraic object (of the same sort)
\[A = \varinjlim_{i}A_i = \bigsqcup_iA_i/\sim\]
where
\[\forall i \leq j, a_i \sim a_j = \phi_{ij}(a_i) \in A_j\]
In the cases we're considering the homomorphisms will be injective, but they don't have to be.
The direct limit comes with a natural homomorphism \(\forall i \in I, \varphi_i: A_i \to A\) with \(\varphi_i = \varphi_j \circ \varphi_{ij}\) for all \(i \leq j\). If each \(A_i\) is also a topological space and the \(\varphi_{ij}\) are continous, the \textbf{direct limit topology} on \(A\) is the \underline{strongest} (largest, most open sets) topology such that all the maps \(\phi_i: A_i \to A\) are continous.
\end{definition}
For example, fix \(p\), \(I = (\nats, \leq)\), where \(m \leq n\) if \(n|m\), and define
\[A_m = \field{p^m}, \quad \phi_{mn} = \field{p^m} \incl{} \field{p^m} \implies \varinjlim_{m}\field{p^m} = \overline{\field{p}}\]
Assigning each \(A_m\) the discrete topology (as a finite set), the direct limit topology is also the discrete topology. The dual limit is what is called the inverse limit: category theorists love this dual stuff.
\begin{definition}[Inverse Limit]
Suppose instead \(\{A_i: i \in I\}\) come with homomorphisms
\(\psi_{ij}: A_j \to A_i\) for \(i \leq j\), again satisfying a transitivity property \(\psi_{ik} = \psi_{ij} \circ \psi_{jk}\) and \(\psi_{ii} = \mb{1}\). In this case the most basic case is when the \(\psi_{ij}\) are surjective, though again this is not necessary.
The \textbf{inverse limit} is defined as
\[A = \varprojlim_iA_i = \left\{a \in \prod_{i \in I}A_i : \forall i \leq j, a_i = \psi_{ij}a_j \right\}\]
If we're dealing with fields, it's a subring of the direct product of these things. This also comes with a natural homomorphism \(\psi_i: A \to A_i\) such that \(\psi_i = \psi_{ij} \circ \psi_j\) for \(i \leq j\). If each \(A_i\) is also a topological space such that the maps \(\psi_{ij}\) are all continuous, the \textbf{inverse limit topology} on \(A\) is the the \underline{weakest} (i.e. smallest, least open sets) such that the maps \(\psi_i: A \to A_i\) are all continuous.
\end{definition}
For example, if \(p\) is fixed and \(I = (\nats, \leq)\) as above, taking
\[A_m = \Gal(\field{p^m}/\field{p}) \simeq \ints/m\ints\]
with the canonical surjection
\[\psi_{mn}: A_m \simeq \ints/m\ints \to A_n \simeq \ints/n\ints\]
in this case we \underline{define}
\[\Gal(\overline{\field{p}}/\field{p}) = A = \varprojlim_{n}A_n\]
We again give each \(A_n\) the discrete topology. Howevwer, the topology on \(A\) is \underline{not} discrete: rather, it is a \underline{closed} subspace of the infinite direct product
\[\prod_{m \in \ints}\Gal(\field{p^m}/\field{p}) = \prod_{m \in \ints}\ints/m\ints\]
Note that this is an infinite, but compact and totally disconnected, topological space by Tychonoff's theorem.

\end{document}
